package com.firebase.fireapp;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {

    EditText editText;
    Button submit;
    DatabaseReference rootRef, demoRef;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        editText = (EditText) findViewById( R.id.etValue );
        submit = (Button) findViewById( R.id.btnSubmit );



        FirebaseApp.initializeApp(this);

        rootRef= FirebaseDatabase.getInstance().getReference();

        demoRef=rootRef.child( "demo" );

        submit.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String  value  = editText.getText().toString();
                demoRef.push().setValue( value );
            }
        } );
    }
}
